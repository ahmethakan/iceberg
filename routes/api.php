<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Models\User;

use App\Http\Controllers\AuthController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get(
    '/login-message',
    [AuthController::class, 'message']
)->name('login');

Route::post(
    '/user-create',
    [AuthController::class, 'register']
);

Route::post(
    '/user-login',
    [AuthController::class, 'login']
);


Route::middleware('auth')->group( function ()
{
    Route::get(
        '/user',
        [AuthController::class, 'user']
    );

    Route::post(
        '/create-appointment',
        [AuthController::class, 'create_appointment']
    );

    Route::delete(
        '/delete-appointment',
        [AuthController::class, 'delete_appointment']
    );

    Route::get(
        '/appointments',
        [AuthController::class, 'get_appointments']
    );

    Route::put(
        '/update-appointment',
        [AuthController::class, 'update_appointment']
    );
});


