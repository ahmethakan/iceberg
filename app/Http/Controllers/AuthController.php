<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Auth;

use App\Models\User;
use App\Models\Appointment;
use App\Models\Contact;
use Validator;


class AuthController extends Controller
{
    // User register function
    public function register(Request $request)
    {
        $user = User::where('email', $request->email)->first();

        if ($user == null)
        {
            $user = new User;

            $user->name = $request->name;
            $user->email = $request->email;
            $user->phone = $request->phone;
            $user->password = md5($request->password);

            $user->save();

            if ($user)
            {
                return response()->json([
                    'message' => 'User successfully registered.',
                ],
                    201
                );
            }
        }

        return response()->json([
            'message' => 'User already registered.',
        ],
            404
        );
    }

    // User login function
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|string',
            'password' => 'required|string',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }
        else
        {
            $user = User::where([
                'email' => $request->email,
                'password' => md5($request->password)
            ])->first();

            if (! $user )
            {
                return response()->json([
                'message' => 'There are no registered users with this e-mail address.'
                    ],
                    401
                );
            }
            else
            {
                $token = auth( "api" )->login( $user );

                return response()->json([
                    'JWT' => $token
                ],
                200
                );

            }
        }
    }

    // Re-login message
    public function message()
    {
        return response()->json([
            'message' => "The JWT may have expired. Please login again."
        ],
        401
        );
    }

    public function create_appointment(Request $request)
    {
        // check auth
        if(Auth::check())
        {
            $contact = Contact::where('email', $request->email)->first();

            if ($contact == null)
            {
                $contact = new Contact;

                $contact->name = $request->name;
                $contact->email = $request->email;
                $contact->phone = $request->phone;

                $contact->save();
            }

            $app = Appointment::where([
                ['contact_id', '=', $contact->id],
                ['address', '=', $request->address],
            ])->first();

            if ($app == null)
            {
                $app = new Appointment;

                $app->user_id = Auth::user()->id;
                $app->contact_id = $contact->id;
                $app->date = $request->date;
                $app->address = $request->address;
                $app->distance = "1";
                $app->estimated_departure = "1";
                $app->estimated_return = "1";

                $app->save();

                return response()->json([
                    'message' => "The appointment was created successfully.",
                    'address' => $request->address,
                    'date' => $request->date,
                    'name' => $request->name,
                    'email' => $request->email,
                    'phone' => $request->phone
                ],
                201
                );
            }

            return response()->json([
                'message' => "The appointment has already been created."
            ],
            404
            );
        }
        else
        {
            return response()->json([
                'message' => "Please re-login."
            ],
            401
            );
        }
    }

    public function delete_appointment(Request $request)
    {
        // check auth
        if(Auth::check())
        {
            $app = Appointment::where([
                ['id', '=', $request->id]
            ])->first();

            if ($app)
            {
                $res=Appointment::where('id',$request->id)->delete();

                if ($res)
                {
                    return response()->json([
                        'message' => 'The appointment deleted.',
                    ],
                        200
                    );
                }
                else
                {
                    return response()->json([
                        'message' => 'Please try again.',
                    ],
                        404
                    );
                }
            }

            return response()->json([
                'message' => 'The appointment not found.',
            ],
                404
            );
        }
        else
        {
            return response()->json([
                'message' => "Please re-login."
            ],
            401
            );
        }
    }

    public function get_appointments(Request $request)
    {
        $app = Appointment::all();

        if ($app)
        {
            return response()->json([
                'appointments' => $app,
            ],
                200
            );
        }
    }

    public function update_appointment(Request $request)
    {
        // get appointment by id
        $app = Appointment::where([
            ['id', '=', $request->id],
        ])->first();

        if ($app)
        {
            $app->date = $request->date;
            $app->address = $request->address;

            $app->save();

            return response()->json([
                'message' => "The appointment was updated successfully.",
                'address' => $request->address,
                'date' => $request->date
            ],
            201
            );
        }
        else
        {
            return response()->json([
                'message' => 'The appointment not found.',
            ],
                404
            );
        }
    }

    public function getRandomPostCodes()
    {
        $response = Http::get('api.postcodes.io/random/postcodes');

        if ($response)
        {
            return $response->object()->result->postcode;
        }
    }

}
