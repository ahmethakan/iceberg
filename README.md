# ICEBERG API documentation

Ahmet Hakan Çelik

Postman Collection -> [iceberg.postman_collection.json](https://gitlab.com/ahmethakan/iceberg/-/blob/master/iceberg.postman_collection.json)

**Google MAP API is not included.**

# REST API

The REST API to the example app is described below.

## Create a new user

### Request

`POST /api/user-create`

    http://127.0.0.1:8000/api/user-create

    {
        "name": "ali",
        "email":"ali@ali.com",
        "phone": "13213",
        "password": "123"
    }

### Response

    Status: 201 Created

    {
        "message": "User successfully registered."
    }

## Login

### Request

`POST /api/user-login`

    http://127.0.0.1:8000/api/user-login

    {
        "email":"ali@ali.com",
        "password": "123"
    }

### Response

    Status: 200 OK

    {
        "JWT": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC8xMjcuMC4wLjE6ODAwMFwvYXBpXC91c2VyLWxvZ2luIiwiaWF0IjoxNjI4MzQwMzcxLCJleHAiOjE2MjgzNDM5NzEsIm5iZiI6MTYyODM0MDM3MSwianRpIjoibGVneHJDSkJIWEpyYWlPYyIsInN1YiI6MywicHJ2IjoiMjNiZDVjODk0OWY2MDBhZGIzOWU3MDFjNDAwODcyZGI3YTU5NzZmNyJ9.Jawvbk9ditv6nXaGTJOTyZ8tl-prkEvvG_wFGpRTvuQ"
    }

## Create Appointment

### Request

`POST /api/create-appointment`

    http://127.0.0.1:8000/api/create-appointment

    Header:
    {
        "Content-Type": application/json
        "Authorization": Bearer JWT_TOKEN
    }

    Body:
    {
        "address": "cm27pj1",
        "date": "01.01.2021",
        "name": "can",
        "email": "can@can",
        "phone": "324234"
    }

### Response

    Status: 201 Created

    {
        "message": "The appointment was created successfully.",
        "address": "cm27pj1",
        "date": "01.01.2021",
        "name": "can",
        "email": "can@can",
        "phone": "324234"
    }

## Delete Appointment

### Request

`DELETE /api/delete-appointment`

    http://127.0.0.1:8000/api/delete-appointment

    Header:
    {
        "Content-Type": application/json
        "Authorization": Bearer JWT_TOKEN
    }

    Body:
    {
        "id": "6"
    }

### Response

    Status: 200 OK

    {
        "message": "The appointment deleted."
    }

## Update Appointment

### Request

`PUT /api/update-appointment`

    http://127.0.0.1:8000/api/update-appointment

    Header:
    {
        "Content-Type": application/json
        "Authorization": Bearer JWT_TOKEN
    }

    Body:
    {
        "id": "9",
        "address": "cm27pj",
        "date": "01.01.2022"
    }

### Response

    Status: 200 OK

    {
        "message": "The appointment was updated successfully.",
        "address": "cm27pj",
        "date": "01.01.2022"
    }

## Get list of Appointments

### Request

`GET /api/appointments`

    http://127.0.0.1:8000/api/appointments

    Header:
    {
        "Content-Type": application/json
        "Authorization": Bearer JWT_TOKEN
    }

### Response

    Status: 200 OK

    {
        "appointments": [
            {
                "id": 7,
                "user_id": 3,
                "contact_id": 5,
                "date": "01.01.2021",
                "address": "cm27pj",
                "distance": "1",
                "estimated_departure": "1",
                "estimated_return": "1",
                "created_at": "2021-08-07T12:11:29.000000Z",
                "updated_at": "2021-08-07T12:11:29.000000Z"
            },
            {
                "id": 9,
                "user_id": 3,
                "contact_id": 5,
                "date": "01.01.2021",
                "address": "cm27pja",
                "distance": "1",
                "estimated_departure": "1",
                "estimated_return": "1",
                "created_at": "2021-08-07T12:54:27.000000Z",
                "updated_at": "2021-08-07T12:54:27.000000Z"
            }
        ]
    }
